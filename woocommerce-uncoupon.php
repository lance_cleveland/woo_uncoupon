<?php
/*
Plugin Name: WooCommerce Uncoupon
Plugin URI: https://www.storelocatorplus.com/
Description: Increase the cart by 10% if a specific product is in the cart.
Author: WooCommerce
Author URI: http://woocommerce.com/
Developer: Lance Cleveland
Developer URI: http://www.lance.bio/
License: GPL3
Tested up to: 4.8
Version: 0.1

Text Domain: woocommerce-uncoupon
Domain Path: /languages

Copyright 2017 Charleston Software Associates (info@charlestonsw.com)
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) || ! defined( 'ABSPATH' ) ) {
	exit;
}

// We do not do any heartbeat processing
if ( defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_POST[ 'action' ] ) && ( $_POST['action'] === 'heartbeat' ) ) {
	return;
}

// Stop if we are we have no WooC.
if ( ! function_exists( 'is_woocommerce_active' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}
if ( ! is_woocommerce_active() ) {
	return;
}


require_once( 'include/base/WCUncoupon.php' );

/**
 * Main instance of WCUncoupon
 *
 * @return WCUncoupon
 */
function WCUncoupon() {
	defined( 'WCUncoupon_PATH' ) || define( 'WCUncoupon_PATH' , plugin_dir_path( __FILE__ ) );
	return WCUncoupon::instance();
}

$GLOBALS['wcuncoupon'] = WCUncoupon();

add_action( 'init', array( $GLOBALS['wcuncoupon'] , 'init' ) );