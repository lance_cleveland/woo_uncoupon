<?php
defined( 'ABSPATH' ) || die( );

if ( ! class_exists( 'WCUncoupon_Cart_Modifier' ) ) {
	/**
	 * Class WCUncoupon_Cart_Modifier
	 */
	class WCUncoupon_Cart_Modifier {
		private $untotal = 0.00;
		private $uncoupon_in_cart = false;

		/**
		 * WCUncoupon_Cart_Modifier constructor.
		 */
		public function __construct() {
			add_filter( 'woocommerce_calculated_total' , [ $this , 'revise_total_to_add_uncoupon' ] , 90 , 2 ); // Calculate Totals On Checkout

			add_filter( 'woocommerce_get_order_item_totals' , [ $this , 'display_uncoupon_in_confirmation' ] , 90 , 3 ); // After checkout confirmation

			add_action( 'woocommerce_cart_totals_before_order_total' , [ $this , 'display_uncoupon_in_cart' ] ); // View Cart
			add_action( 'woocommerce_review_order_before_order_total' , [ $this , 'display_uncoupon_in_cart' ] ); // Checkout

		}

		/**
         * Display the uncoupon row.
         *
		 * @param array     $total_rows     The rows for the totals.
		 * @param WC_Order  $order          The order
		 * @param mixed     $tax_display    Tax stuff
         * @return array                    Modified total rows.
		 */
		public function display_uncoupon_in_confirmation( $total_rows, $order, $tax_display ) {
		   if ( $this->order_has_uncoupon( $order ) ) {

		       // Pop off the order total line
		       $saved_total = $total_rows['order_total'];
		       unset( $total_rows['order_total'] );

		       // Add the uncoupon line
		       $total_rows[ 'uncoupon' ] = array(
		           'label' => __( 'Uncoupon' , 'woocommerce-uncoupon' ),
                   'value' => wc_price( $this->get_untotal( $order->get_subtotal() ) ),
               );

		       // put the total back
		       $total_rows['order_total'] = $saved_total;
           }
		   return $total_rows;
        }

		/**
		 * Calculate the 10% price bump.
         *
         * @param float $cart_total
         * @return float
		 */
		private function get_untotal( $cart_total ) {
			$this->untotal =  $cart_total * 0.10;
			return $this->untotal;
		}

		/**
		 * Scan the cart items for the uncoupon product ID.
		 */
		private function cart_has_uncoupon( ) {
			$this->uncoupon_in_cart = false;

			if ( ! empty( WC()->cart->cart_contents ) ) {
				foreach ( WC()->cart->cart_contents as $cart_item ) {
					if ( $this->is_uncoupon( $cart_item['data']->get_id() ) ) {
						$this->uncoupon_in_cart = true;
						break;
					}
				}
			}

			return $this->uncoupon_in_cart;
		}


		/**
		 * Scan the order items for the uncoupon product ID.
         *
		 * @param WC_Order $order
		 *
		 * @return bool
		 */
		private function order_has_uncoupon( $order ) {
			$this->uncoupon_in_cart = false;

			/**
			 * @var WC_Order_Item_Product $cart_item
			 */
            foreach ( $order->get_items() as $cart_item ) {
                if ( $this->is_uncoupon( $cart_item->get_product_id() ) ) {
                    $this->uncoupon_in_cart = true;
                    break;
                }
            }

			return $this->uncoupon_in_cart;
		}

		/**
         * Is the provided woocommerce line item the uncoupon?
         *
		 * @param int   $product_id
		 *
		 * @return bool                 true if this item is the uncoupon product
		 */
		private function is_uncoupon( $product_id ) {
		    return ( $product_id == (int) get_option( 'wunc_product_id' , '' ) );
        }

		/**
		 * Display uncoupon line in cart.
		 */
		public function display_uncoupon_in_cart() {
		    if ( ! $this->cart_has_uncoupon() ) {
		        return;
		    }
?>
			<tr class="order-uncoupon">
				<th><?php _e( 'Uncoupon', 'woocommerce-uncoupon' ); ?></th>
				<td data-title="<?php esc_attr_e( 'Uncoupon', 'woocommerce-uncoupon' ); ?>"><?php $this->display_untotal(); ?></td>
			</tr>
<?php
		}

		/**
		 * Display the untotal line.
		 */
		public function display_untotal() {
			echo wc_price( $this->untotal );
		}

		/**
         * Revise total to include uncoupon.
         *
		 * @param $total
		 * @param $cart
		 *
		 * @return mixed
		 */
		public function revise_total_to_add_uncoupon( $total , $cart ) {
		    if ( ! $this->cart_has_uncoupon() ) {
		        return $total;
            }
            return $total + $this->get_untotal( WC()->cart->get_displayed_subtotal() );
        }
	}

	/**
	 * @var WCUncoupon $wcuncoupon
	 */
	global $wcuncoupon;
	if ( empty( $wcuncoupon->objects['Cart_Modifier'] ) ) {
		$wcuncoupon->objects['Cart_Modifier'] = new WCUncoupon_Cart_Modifier();
	}
}