<?php
defined( 'ABSPATH' ) || die( 'You cannot access this page directly.' );
if ( ! class_exists( 'WCUncoupon' ) ) :

	/**
	 * Class WCUncoupon
	 *
	 * @property    WCUncoupon  $_instance
	 * @property    array       $objects        Instantiated objects, gives them an easily-referenced address
	 */
	class  WCUncoupon {
	    protected static $_instance = null;
		public $objects;


	    /**
	     * Main WCUncoupon Instance.
	     *
	     * Ensures only one instance of WCUncoupon is loaded or can be loaded.
	     *
	     * @since 0.1
	     * @static
	     * @see WCUncoupon()
	     * @return WCUncoupon - Main instance.
	     */
	    public static function instance() {
	        if ( is_null( self::$_instance ) ) {
	            self::$_instance = new self();
	        }
	        return self::$_instance;
	    }

		/**
		* Do this on WP Init
		*/
		public function init() {
	        $this->init_hooks();
		}

		/**
		* Setup hooks and filters for this plugin.
		*/
		private function init_hooks() {
			require_once(WCUncoupon_PATH. '/include/cart/WCUncoupon_Cart_Modifier.php' );

			// Admin specific hooks
			if ( is_admin() ) {
				require_once(WCUncoupon_PATH. '/include/admin/WCUncoupon_Admin.php' );
			}
		}

	}
endif;
