<?php
defined( 'ABSPATH' ) || die( );

if ( ! class_exists( 'WCUncoupon_Admin' ) ) {
	/**
	 * Class WCUncoupon_Admin
	 */
	class WCUncoupon_Admin {
		private $tab_slug = 'uncoupons';

		/**
		 * WCUncoupon_Admin constructor.
		 */
		public function __construct() {
			add_filter( 'woocommerce_settings_tabs_array'               , [ $this , 'add_settings_tab'      ] , 60  );  // Add tab to WC settings
			add_action( 'woocommerce_settings_' . $this->tab_slug       , [ $this , 'display_settings_tab'  ]       );  // Render the settings tab
			add_action( 'woocommerce_update_options_' . $this->tab_slug , [ $this , 'save_settings'         ]       );  // Save the settings
		}

		/**
		 * Add the Uncoupon settings tab to the WooCommerce settings tabs array.
		 *
		 * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Uncoupon tab.
		 * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Coupon tab.
		 * @since 0.1
		 */
		public function add_settings_tab( $settings_tabs ) {
			$settings_tabs[ $this->tab_slug ] = __( 'Uncoupons', 'woocommerce-uncoupon' );
			return $settings_tabs;
		}

		/**
		 * Display the settings tab.
		 *
		 * @uses get_settings
		 *
		 * @since 0.1
		 */
		public function display_settings_tab() {
			woocommerce_admin_fields( $this->get_settings() );
			wp_nonce_field( 'wunc_' . $this->tab_slug . '_settings', 'wunc_nonce', false );
		}

		/**
		 * Get our settings.
		 *
		 * @used-by display_settings_tab
		 * @used-by save_settings
		 *
		 * @since 0.1
		 */
		public function get_settings() {
			return array(
				array(
					'name'      => __( 'Product ID' , 'woocommerce-uncoupon' ),
					'type'      => 'text' ,
					'desc'      => __( 'Enter the product ID that will trigger a 10% total cart increase.', 'woocommerce-uncoupon' ),
					'desc_tip'  => true,
					'id'        => 'wunc_product_id',

				),
			);
		}

		/**
		 * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
		 *
		 * @uses woocommerce_update_options
		 * @uses get_settings
		 * @since 0.1
		 */
		public function save_settings() {
			if ( empty( $_POST['wunc_nonce'] ) || ! wp_verify_nonce( $_POST['wunc_nonce'], 'wunc_' . $this->tab_slug . '_settings' ) ) {
				return;
			}
			woocommerce_update_options( $this->get_settings() );
		}
	}

	/**
	 * @var WCUncoupon $wcuncoupon
	 */
	global $wcuncoupon;
	if ( empty( $wcuncoupon->objects['Admin'] ) ) {
		$wcuncoupon->objects['Admin'] = new WCUncoupon_Admin();
	}
}