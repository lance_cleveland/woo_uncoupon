=== WooCommerce Uncoupon ===
Plugin Name:  WooCommerce Uncoupon
Contributors: charlestonsw
Donate link: https://www.storelocatorplus.com/
Tags: woocommerce, coupon, not
Requires at least: 4.4
Tested up to: 4.8
Stable tag: 0.1

Increase the cart by 10% if a specific product is in the cart.

== Description ==

Increase the cart by 10% if a specific product is in the cart.

== Installation ==

= Requirements =

* WordPress: 4.4
* WooCommerce: 3.0
* PHP: 5.2

= Install After WooCommerce =

1. Go to plugins/add new and select upload.
2. Upload the woocommerce-uncoupon.zip file and activate it.

== Frequently Asked Questions ==

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you
wish.

== Changelog ==

= 0.1 =

Initial private release.